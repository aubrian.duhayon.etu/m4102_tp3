# Structure du fichier JSON des pizzas

~~~json
{
	"id": "f38806a8-7c85-49ef-980c-149dcd81d307", 
	"name": "pizzaTest", 
	"ingredients": ["mozzarella", "jambon"]
}
~~~

La création doit se faire à partir du `name` et de la `liste d'ingrédients`.

L'`id` est fourni automatiquement à la création du JavaBean

~~~json
{
	"name": "pizzaTest", 
	"ingredients": ["mozzarella", "jambon"]
}
~~~



#Première implémentation : récupérer les pizzas existantes et tester les requêtes sur les pizzas qui n'existent pas

~~~java

public class PizzaResourceTest extends JerseyTest {
	
	private PizzaDao dao;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		 
		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
		dao.createPizzaTable();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTable();
	}
	
	@Test
	public void getExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("");
		dao.insert(pizza);
			
		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		Ingredient result = Ingredient.fromDto(response.readEntity(IngredientDto.class));
		assertEquals(pizza, result);
	}
	
		@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}
}
~~~

Il faut donc implémenter la classe `Pizza`, ainsi que les méthodes `insert()` et `dropTable()` dans la classe `PizzaDao`

~~~java

public class Pizza {
	
	private String name;
	private UUID id = UUID.randomUUID();
	private List<String> ingredients;
	
	public Pizza() {}
	
	public Pizza(String name) {
		this(name, null, null);
	}

	public Pizza(String name, UUID id) {
		this(name, id, new ArrayList<>());
	}
	
	public Pizza(String name, UUID id, List<String> ingredients) {
		this.name = name;
		this.id = id;
		this.ingredients = ingredients;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public List<String> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<String> ingredients) {
		this.ingredients = ingredients;
	}	
}

~~~

###Méthodes insert et dropTables de la classe PizzaDao

~~~java
  @SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
  void insertPizza(@BindBean Pizza pizza);
  
  @SqlUpdate("INSERT INTO PizzaIngredientAssociation (id_ingredient, id_pizza) VALUES (:id_ingredient, :id_pizza)")
  void insertPizzaAssociation(@Bind(":id_pizza") String id_pizza, @Bind(":id_ingredient") String id_ingredient);
  
  @Transaction
  default void insert(Pizza pizza, List<Ingredient> ingredients) {
	  insertPizza(pizza);
	  for(Ingredient i : ingredients) {
		  insertPizzaAssociation(pizza.getId().toString(), i.getId().toString());	
	  }
  }
  
   @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
  void dropPizzaTable();
  
  @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
  void dropAssociationTable();
  
  @Transaction
  default void dropTables() {
	  dropPizzaTable();
	  dropAssociationTable();
  }
~~~

# 2ème implémentation : Créer une pizza, et s'assurer qu'on ne peut pas créer plusieurs fois la même pizza

~~~java
	@Test
		public void testCreateSamePizza() {
			Pizza pizza = new Pizza();
			pizza.setName("test");
			pizza.setIngredients(new ArrayList<>());
			dao.insertPizza(pizza);
	
			PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
			Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));
	
			assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
		}
		
		@Test
		public void testCreatePizzaWithoutName() {
			PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
	
			Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));
	
			assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
		}
~~~

##### Il faut maintenant créer les méthodes permettant de créer une pizza automatiquement à partir du DTO
A la suite de la class `Pizza`, il faut ajouter les méthodes ci-dessous


~~~java
public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setId(p.getId());
		dto.setName(p.getName());
		dto.setIngredients(p.getIngredients());

		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());
		pizza.setIngredients(dto.getIngredients());

		return pizza;
	}
	
	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizza.getName());
		dto.setIngredients(pizza.getIngredients());
		
		return dto;
	}
	
	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());
		pizza.setIngredients(dto.getIngredients());

		return pizza;
	}
~~~

La classe `PizzaDTO` est une copie du POJO Pizza, sans les constructeurs et les méthodes ajoutées juste avant.

La classe `PizzaCreateDTO` est une copie de PizzaDTO sans les getter et setter de l'id


~~~java
public class PizzaDto {
	
	private UUID id;
	private String name;
	private List<String> ingredients;
	
	public PizzaDto() {
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<String> list) {
		this.ingredients = list;
	}
}
~~~

~~~java
public class PizzaCreateDto {
	private String name;
	private List<String> ingredients;
	
	public PizzaCreateDto() {}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}

	public List<String> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<String> map) {
		this.ingredients = map;
	}
}
~~~

#3ème implémentation : La suppression 