package fr.ulille.iut.pizzaland.dto;

import java.util.List;

public class PizzaCreateDto {
	private String name;
	private List<String> ingredients;
	
	public PizzaCreateDto() {}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}

	public List<String> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<String> map) {
		this.ingredients = map;
	}
}
