package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
  @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id varchar(128) PRIMARY KEY, name varchar UNIQUE NOT NULL)")
  void createPizzaTable();

  @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (id_pizza VARCHAR(128) NOT NULL, id_ingredient VARCHAR(128) NOT NULL)")
  void createAssociationTable();

  @Transaction
  default void createTableAndIngredientAssociation() {
	  createPizzaTable();
	  createAssociationTable();
  }

  @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
  void dropPizzaTable();
  
  @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
  void dropAssociationTable();
  
  @Transaction
  default void dropTables() {
	  dropPizzaTable();
	  dropAssociationTable();
  }
  
  @SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
  void insertPizza(@BindBean Pizza pizza);
  
  @SqlUpdate("INSERT INTO PizzaIngredientsAssociation (id_pizza, id_ingredient) VALUES (:id_pizza, :id_ingredient)")
  void insertPizzaAssociation(@Bind("id_pizza") UUID id_pizza, @Bind("id_ingredient") UUID id_ingredient);
  
  @Transaction
  default void insert(Pizza pizza, List<UUID> idIngredients) {
	  insertPizza(pizza);
	  for(UUID i : idIngredients) {
		  insertPizzaAssociation(pizza.getId(), i);	
	  }
  }
  
  @SqlQuery("SELECT * FROM Pizzas")
  @RegisterBeanMapper(Pizza.class)
  List<Pizza> getAll(); 
  
  @SqlQuery("SELECT name FROM Ingredients WHERE id IN (SELECT id_ingredient FROM PizzaIngredientsAssociation WHERE id_pizza = :idPizza)")
  List<String> getIngredientsPizza(@Bind("idPizza") UUID idPizza);
  
  @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
  @RegisterBeanMapper(Pizza.class)
  Pizza findById(@Bind("id") UUID id);
  
  @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
  @RegisterBeanMapper(Pizza.class)
  Pizza findByName(@Bind("name") String name);
  
  @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
  void remove(@Bind("id") UUID id);
}